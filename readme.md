# Chapter 8 - Challenge

## API Documentation (Swagger) and React SPA

---

### How to run project

#### REACT APP

change current directory to client

`cd client`

install dependencies

`npm run install`

run the app and server will be ready at port 3000

`npm run start`

#### REST API

change current directory to server

`cd server`

install dependencies

`npm run install`

run the app and server will be ready at port 5000

`npm run start`

Swagger API Documentation Endpoint

> localhost:5000/api-docs
