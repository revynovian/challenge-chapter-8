import './App.css';
import { useState } from 'react';
import CreatePlayer from './components/form/createPlayer';
import PlayerList from './components/table/playerList';
import UpdatePlayer from './components/form/updatePlayer'

const App = () => {
  const [playerList, setPlayerlist] = useState([])
  const [updateId, setUpdateId] = useState(null)
  const [updateData, setUpdate] = useState({
    username : '',
    email : '',
    exp : '',
    level : ''
  })

  const eventCreatePlayer = (newPlayer) => {
    setPlayerlist([...playerList, newPlayer])
  }

  const eventUpdatePlayer = (updatedPlayer) => {
    let newPlist = [...playerList]
    newPlist[updateId] = updatedPlayer
    setPlayerlist(newPlist)
  }
  const handleUpdate = (id, data) => {
    setUpdate(data)
    setUpdateId(id)
  }

  return (
    <main className="container">
      <div className="row justify-content-center">
        <div className="d-flex p-4">
        <i className="fas fa-home p-1"></i>
        <h2>Dashboard</h2>
        </div>          
        <div className="col-4 d-flex flex-column p-5">
          <CreatePlayer onCreate={eventCreatePlayer}/>
          <UpdatePlayer dataUpdate={updateData} onUpdate={eventUpdatePlayer}/>
        </div>
        <div className="col-7 p-5">
          <PlayerList dataPlayer={playerList} selectData={handleUpdate}/>
        </div>
      </div>
    </main>
  );
}

export default App;
