import './playerList.css'

const PlayerList = ({dataPlayer, selectData}) => {

  return (
    <section className="board-table">
      <div className="d-flex align-items-center badge-custom">
        <i className="fas fa-list"></i>
        <h2>Player List</h2>
      </div>
      <table className="table table-custom">
        <thead>
          <tr>
            <th scope="col">username</th>
            <th scope="col">email address</th>
            <th scope="col">exprerience</th>
            <th scope="col">level</th>
            <th scope="col">More</th>
          </tr>
        </thead>
        <tbody>
          {
            dataPlayer.map((item,i) => {
              return(
                <tr key={i}>
                  <th className="align-middle" scope="row">{item.username}</th>
                  <td className="align-middle">{item.email}</td>
                  <td className="align-middle">{item.exp}</td>
                  <td className="align-middle">{item.level}</td>
                  <td className="align-middle">
                    <button type="button" className="btn btn-success badge rounded-pill" onClick={()=> {selectData (i, item)}}>Update</button>
                  </td>
                </tr>)
            })
          }       
        </tbody>
      </table>
    </section>
  )
}

export default PlayerList;