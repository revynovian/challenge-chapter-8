import './form-style.css'
import {  useState, useEffect } from 'react'

const UpdatePlayer = ({dataUpdate, onUpdate}) => {
  // prop value from app component
  const { username, email, exp, level} = dataUpdate

  const [Username, setUsername] = useState(username)
  const [Email, setEmail] = useState(email)
  const [Exp, setExp] = useState(exp)
  const [Level, setLevel] = useState(level)
  
  useEffect(()=> {
    setUsername(username)
    setEmail(email)
    setExp(exp)
    setLevel(level)
  },[username, email, exp,level])
  

  const submitHandler = (e) => {
    e.preventDefault()
    const updatedPlayer = {
      username : Username,
      email : Email,
      exp : Exp,
      level : Level
    }
    // send updated data to app(parent) component
    onUpdate(updatedPlayer)
  }
  
  return (
    <form onSubmit={submitHandler}>
      <div className="card">
        <div className="card-header d-flex flex-column align-items-center p-1">
          <h2 className="my-4">Update User</h2>
        </div>
        <div className="card-body">
          <div className="my-4">
            <input type="text" className="form-control" value={Username} id="username" placeholder="username" required name="username" onChange={(e) => setUsername(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="email" className="form-control"  value={Email} id="email" placeholder="email address" required name="email" onChange={(e) => setEmail(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="text" className="form-control"  value={Exp} name="experience" placeholder="exprerience" onChange={(e) => setExp(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="text" className="form-control"  value={Level} name="level" placeholder="level" onChange={(e) => setLevel(e.target.value)}/>
          </div>
        </div>
        <button type="submit" className="btn btn--custom">
          Save Changes
        </button>
      </div>
    </form>
  );
};

export default UpdatePlayer;
