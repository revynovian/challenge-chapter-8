import './form-style.css'
import {  useState } from 'react'


const CreatePlayer = ({onCreate}) => {

  const [Username, setUsername] = useState('')
  const [Email, setEmail] = useState('')
  const [Exp, setExp] = useState('')
  const [Level, setLevel] = useState('')

  const submitHandler = (e) => {
    e.preventDefault()
    const newPlayer = {
      username : Username,
      email : Email,
      exp : Exp,
      level : Level
    }    
    onCreate(newPlayer)

    // remove form value after submit
    // setUsername('')
    // setEmail('')
    // setExp('')
    // setLevel('')
  }

  return (
    <form onSubmit={submitHandler}>
      <div className="card mb-5">
        <div className="card-header d-flex flex-column align-items-center p-1">
          <h2 className="my-4">Create User</h2>
        </div>
        <div className="card-body">
          <div className="my-4">
            <input type="text" className="form-control" value={Username} id="username" placeholder="username" required name="username" onChange={(e) => setUsername(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="email" className="form-control" value={Email} id="email" placeholder="email address" required name="email" onChange={(e) => setEmail(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="text" className="form-control" value={Exp} name="experience" placeholder="exprerience" onChange={(e) => setExp(e.target.value)}/>
          </div>
          <div className="my-4">
            <input type="text" className="form-control" value={Level} name="level" placeholder="level" onChange={(e) => setLevel(e.target.value)}/>
          </div>
        </div>
        <button type="submit" className="btn btn--custom">
          Create
        </button>
      </div>
    </form>
  );
};

export default CreatePlayer;
