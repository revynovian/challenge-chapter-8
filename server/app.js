const express = require("express");
const cors = require("cors");

const app = express();

// api-docs setup
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./app/docs/swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// accept request in form or JSON
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const db = require("./app/models");
db.client.sync();

require("./app/routes/player.routes")(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
